// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers' ])
/* //if use wakanda platform 
    angular.module('starter', ['ionic', 'starter.controllers','wakanda'])
*/
.run(function($ionicPlatform ,$rootScope ,$location,$ionicModal,$ionicSlideBoxDelegate,$ionicPopup) {
    
    
$rootScope.showImg=function(img){
    $rootScope.bigImg=img
}  
/*************** zoom function ****************/
   $rootScope.openPhotoSwipe = function(image) {  
        var index=0;
      
        $rootScope.PhotoSwipe=true;
        var pswpElement = document.querySelectorAll('.pswp')[0];
        var items = []
        items.push({ src:image,w:400,h:300})
        var options = {history:false,focus:false,showAnimationDuration:0,hideAnimationDuration:0};
        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options,index);
        gallery.init();              
    }
     
/*************** location function ****************/ 
  $rootScope.goto=function(url){
        $location.path(url)
    }  
  
/*************** active function ****************/ 
  $rootScope.active_icon=false;   
  $rootScope.activeselect=function(index){
      if(index==1){$rootScope.active_icon1=! $rootScope.active_icon1;
                  }
      else if (index==2){
          $rootScope.active_icon2=! $rootScope.active_icon2;
              }
      else {$rootScope.active_icon3=! $rootScope.active_icon3;
           }
  }
  $rootScope.likeItem=function(item){
      item.fav=!item.fav;
  }
 /*************** increment-decrement function ****************/ 
  $rootScope.increment = function(count,index) {
    if (count >= 0) $rootScope.cart[index].count=count+1;
  };
  $rootScope.decrement = function(count,index) {
  if (count >= 1) $rootScope.cart[index].count=count-1;
  };    
 /*************** delete function ****************/   
 $rootScope.removeItem = function (index) {
   $rootScope.cart.splice(index, 1);
 };  
    
$rootScope.removeItemWish = function (index) {
   $rootScope.wish.splice(index, 1);
 };      
  
/*************** increment-decrement function ****************/
 $rootScope.value=1;

 $rootScope.increment_val= function() {
   if ($rootScope.value >= 1) 
   $rootScope.value++;
 };
 $rootScope.decrement_val = function() {
   if ($rootScope.value > 1) 
   $rootScope.value--;
 };    
   
    
$rootScope.showSecondpage=function(index){
    $rootScope.show_step=index;
}    
       
/*************** groups function ****************/     
  $rootScope.groups = [
    { name: 'WESTERN WEAR', id: 1, items: [{ subName: 'SubBubbles1', subId: 'TOPS & TEES' }, { subName: 'SubBubbles2', subId: 'DRESSES & JUMPSUITS' }, { subName: 'SubBubbles2', subId: 'JEANS' }, { subName: 'SubBubbles2', subId: 'SHORTS & SKIRTS' }, { subName: 'SubBubbles2', subId: 'TUNICS' }]},
      
    { name: 'FOOTWEAR', id: 1, items: [{ subName: 'SubBubbles1', subId: 'Boots' }, { subName: 'SubBubbles2', subId: 'Shoes' }, { subName: 'SubBubbles2', subId: 'Sandals' }, { subName: 'SubBubbles2', subId: 'Indoor footwear' }, { subName: 'SubBubbles2', subId: 'Specific footwear' }]},
      
    { name: 'LINGERIE ', id: 1, items: [{ subName: 'SubBubbles1', subId: 'long LINGERIEs' }, { subName: 'SubBubbles2', subId: 'short LINGERIEs' }, { subName: 'SubBubbles2', subId: 'upper LINGERIEs' }, { subName: 'SubBubbles2', subId: 'under LINGERIEs' }]},
      
    { name: 'WATCHES & ACCESSORIES', id: 1, items: [{ subName: 'SubBubbles1', subId: 'Automatic Watches' }, { subName: 'SubBubbles2', subId: 'Chronograph Watches' }, { subName: 'SubBubbles2', subId: 'Couple Watches' }, { subName: 'SubBubbles2', subId: 'Diver Watches' }, { subName: 'SubBubbles2', subId: 'Fashion Watches' }]}
  ];
 
    
    /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $rootScope.toggleGroup = function(group) {
    if ($rootScope.isGroupShown(group)) {
      $rootScope.shownGroup = null;
    } else {
      $rootScope.shownGroup = group;
    }
  };
  $rootScope.isGroupShown = function(group) {
    return $rootScope.shownGroup === group;
  };   
    
    /********************* popup forget password **********************/   
    $rootScope.forget_password=function (){	
        $ionicPopup.show({
        template: 'Enter your email address below.<label class="item item-input" style="  height: 34px; margin-top: 10px;"><input  type="email"  /></label>',
        title: 'Forget Password',
        subTitle: ' ',
        scope: $rootScope,
        buttons: [
        {text: '<b>Send</b>',
        type: 'button-positive'},
        { text: 'Cancel' ,
        type: 'button-positive'},]
        });	
    };
    
  /********************* popup enter-email **********************/   
    $rootScope.enter_email=function (){	
        $ionicPopup.show({
        template: 'Enter your email address below.<label class="item item-input" style="  height: 34px; margin-top: 10px;"><input  type="email"  /></label>',
        title: 'Your Mail',
        subTitle: ' ',
        scope: $rootScope,
        buttons: [
        {text: '<b>Send</b>',
        type: 'button-positive'},
        { text: 'Cancel' ,
        type: 'button-positive'},]
        });	
    };
    
    
  /********************* repeat lists **********************/  
    $rootScope.list_1=[{id:"1",img:"img/img1.png",title:"CASUAL WEAR",price:"215 $"},
                       {id:"2",img:"img/img2.png",title:"CASUAL WEAR",price:"215 $"},
                       {id:"3" ,img:"img/img3.png",title:"CASUAL WEAR",price:"215 $"},
                       {id:"4",img:"img/img4.png",title:"CASUAL WEAR",price:"250$"},
                       {id:"5",img:"img/img1.png",title:"CASUAL WEAR",price:"250$"},
                       {id:"6" ,img:"img/img2.png",title:"CASUAL WEAR",price:"250$"},
                       {id:"7",img:"img/img3.png",title:"CASUAL WEAR",price:"250$"},
                       {id:"8",img:"img/img4.png",title:"CASUAL WEAR",price:"250$"},
                       {id:"9" ,img:"img/img1.png",title:"CASUAL WEAR",price:"250$"},
                       {id:"10" ,img:"img/img2.png",title:"CASUAL WEAR",price:"250$"}]
    
    $rootScope.store_list=[{id:"1",bg:"img/store1.png",logo:"img/store-logo.png"},
                           {id:"2",bg:"img/store2.png",logo:"img/store-logo2.png"},
                           {id:"3",bg:"img/store1.png",logo:"img/store-logo.png"},
                           {id:"4",bg:"img/store2.png",logo:"img/store-logo2.png"},
                           {id:"5",bg:"img/store1.png",logo:"img/store-logo.png"},
                           {id:"6",bg:"img/store2.png",logo:"img/store-logo2.png"}]
    
    $rootScope.cart=[{id:"1",bg:"img/img1.png",count:1},
                     {id:"2",bg:"img/img2.png",count:1},
                     {id:"3",bg:"img/img3.png",count:1}]
    
    
    $rootScope.wish=[{id:"1",bg:"img/img1.png"},
                     {id:"2",bg:"img/img2.png"},
                     {id:"3",bg:"img/img3.png"},
                     {id:"4",bg:"img/img4.png"}]
    
      
    $rootScope.images=[{id:"1",bg:"img/det-head.png"},
                     {id:"2",bg:"img/01.png"},
                     {id:"3",bg:"img/02.png"},
                     {id:"4",bg:"img/03.png"}]
    $rootScope.showImg($rootScope.images[0].bg)
    
/********************* menu-modal **********************/
    
    $ionicModal.fromTemplateUrl('templates/menumodal.html',function(modal){
	$rootScope.menumodal=modal;
	}, {
		scope: $rootScope,
		animation: 'slide-in-up'
	});
	
	$rootScope.openmenumodal= function(){
		$rootScope.menumodal.show();
	};
	
	$rootScope.closemenumodal= function() {	
		$rootScope.menumodal.hide();
	};
	$rootScope.$on('$destroy', function() {
		$rootScope.menumodal.remove();
	});
	$rootScope.$on('modal.hidden', function() {
    // Execute action
  });
    
  /********************* sort-modal **********************/  
    $ionicModal.fromTemplateUrl('templates/sortmodal.html',function(modal){
	$rootScope.sortmodal=modal;
	}, {
		scope: $rootScope,
		animation: 'slide-in-up'
	});
	
	$rootScope.opensortmodal= function(){
		$rootScope.sortmodal.show();
	};
	
	$rootScope.closesortmodal= function() {	
		$rootScope.sortmodal.hide();
	};
	$rootScope.$on('$destroy', function() {
		$rootScope.sortmodal.remove();
	});
	$rootScope.$on('modal.hidden', function() {
    // Execute action
  });
    
  /********************* search-modal **********************/ 
    $ionicModal.fromTemplateUrl('templates/searchmodal.html',function(modal){
	$rootScope.searchmodal=modal;
	}, {
		scope: $rootScope,
		animation: 'slide-in-up'
	});
	
	$rootScope.opensearchmodal= function(){
		$rootScope.searchmodal.show();
	};
	
	$rootScope.closesearchmodal= function() {	
		$rootScope.searchmodal.hide();
	};
	$rootScope.$on('$destroy', function() {
		$rootScope.searchmodal.remove();
	});
	$rootScope.$on('modal.hidden', function() {
    // Execute action
  });
     
 /********************* complete-modal **********************/
    
 $ionicModal.fromTemplateUrl('templates/completemodal.html',function(modal){
	$rootScope.completemodal=modal;
	}, {
		scope: $rootScope,
		animation: 'slide-in-up'
	});
	
	$rootScope.opencompletemodal= function(){
		$rootScope.completemodal.show();
	};
	
	$rootScope.closecompletemodal= function() {	
		$rootScope.completemodal.hide();
	};
	$rootScope.$on('$destroy', function() {
		$rootScope.completemodal.remove();
	});
	$rootScope.$on('modal.hidden', function() {
    // Execute action
  });
    
    
    
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
    
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.backButton.text('').previousTitleText('');

    
  $stateProvider
  
  .state('login', {
    url: "/login",
        templateUrl: "templates/login.html"
  })
  
  .state('register', {
    url: "/register",
        templateUrl: "templates/register.html"
  })
  
  .state('signin', {
    url: "/signin",
        templateUrl: "templates/signin.html"
  })
  
  .state('welcome_page', {
    url: "/welcome_page",
        templateUrl: "templates/welcome_page.html"
  })

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })



  .state('app.hot_deals', {
    url: "/hot_deals",
    views: {
      'menuContent': {
        templateUrl: "templates/hot_deals.html"
      }
    }
  })
    .state('app.categories', {
      url: "/categories",
      views: {
        'menuContent': {
          templateUrl: "templates/categories.html"
        }
      }
    })
  
  .state('app.details', {
      url: "/details",
      views: {
        'menuContent': {
          templateUrl: "templates/details.html"
        }
      }
    })
  
  .state('app.cart', {
      url: "/cart",
      views: {
        'menuContent': {
          templateUrl: "templates/cart.html"
        }
      }
    })
  
  .state('app.wish_list', {
      url: "/wish_list",
      views: {
        'menuContent': {
          templateUrl: "templates/wish_list.html"
        }
      }
    })
  
  .state('app.payment', {
      url: "/payment",
      views: {
        'menuContent': {
          templateUrl: "templates/payment.html"
        }
      }
    })
  
  .state('app.store', {
      url: "/store",
      views: {
        'menuContent': {
          templateUrl: "templates/store.html"
        }
      }
    })
  
  .state('app.profile', {
      url: "/profile",
      views: {
        'menuContent': {
          templateUrl: "templates/profile.html"
        }
      }
    })
  
  .state('app.filter_list', {
      url: "/filter_list",
      views: {
        'menuContent': {
          templateUrl: "templates/filter_list.html"
        }
      }
    })

  .state('app.single', {
    url: "/playlists/:playlistId",
    views: {
      'menuContent': {
        templateUrl: "templates/playlist.html",
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});
